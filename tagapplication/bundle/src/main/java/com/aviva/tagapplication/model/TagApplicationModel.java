/**
 * @Wipro Rights
 */
package com.aviva.tagapplication.model;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aviva.tagapplication.pojos.TaggedLinkPage;
import com.aviva.tagapplication.service.TaggedLinkListService;
import com.day.cq.tagging.Tag;

@Model(adaptables = { Resource.class })
public class TagApplicationModel {

	private final Logger LOGGER = LoggerFactory.getLogger(TagApplicationModel.class);

	@OSGiService
	TaggedLinkListService taggedLinkList;

	@Inject
	@Default(values = "true")
	private String resultIncludeAllTags;

	@Inject
	private String[] tags;

	@Inject
	private ResourceResolver resolver;

	private Resource resource;

	private List<TaggedLinkPage> taggedLinkPage;

	private String[] eachTag;

	@PostConstruct
	private void getPageResult() {

		LOGGER.info("TaggedApplicationModel getPageResult Method Started");
		eachTag = new String[tags.length];
		if (tags != null && tags.length > 0) {
			for (int i = 0; i < tags.length; i++) {
				resource = resolver.getResource(tags[i]);
				if (resource != null) {
					eachTag[i] = resource.adaptTo(Tag.class).getTagID();
				}
			}
		}

		taggedLinkPage = taggedLinkList.getTagList(eachTag, resultIncludeAllTags, resolver);
		LOGGER.info("TaggedApplicationModel getPageResult Method Ended");
	}

	/**
	 * Returns list of tags
	 */
	public String[] getTags() {
		return tags;
	}
	/**
	 * Returns page details
	 */
	public List<TaggedLinkPage> getTaggedLinkPage() {
		return taggedLinkPage;
	}
	/**
	 * Returns each tag selected
	 */
	public String[] getEachTag() {
		return eachTag;
	}
}
