/**
 * @Wipro Rights
 */
package com.aviva.tagapplication.pojos;

/**
 * Pojo named TaggedLinkPage
 */
public class TaggedLinkPage {

	private String pageTitle;
	private String pagePath;
	private String[] tags;

	// default constructor
	public TaggedLinkPage() {

	}

	// Parameterized constructor
	public TaggedLinkPage(String pageTitle, String pagePath, String[] tags) {
		this.pageTitle = pageTitle;
		this.pagePath = pagePath;
		this.tags = tags;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getPagePath() {
		return pagePath;
	}

	public void setPagePath(String pagePath) {
		this.pagePath = pagePath;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}
}
