package com.aviva.tagapplication.service;

import java.util.List;

import org.apache.sling.api.resource.ResourceResolver;

import com.aviva.tagapplication.pojos.TaggedLinkPage;
/**
 * Interface (Service) named TaggedLinkListService
 */
public interface TaggedLinkListService {
	
	public List<TaggedLinkPage> getTagList(String[] taglist, String includeAllTags, ResourceResolver resolver);

}
