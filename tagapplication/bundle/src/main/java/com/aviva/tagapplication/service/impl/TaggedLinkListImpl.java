/**
* @Wipro Rights
 */
package com.aviva.tagapplication.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aviva.tagapplication.pojos.TaggedLinkPage;
import com.aviva.tagapplication.service.DayCalculator;
import com.aviva.tagapplication.service.TaggedLinkListService;
import com.day.cq.commons.RangeIterator;
import com.day.cq.tagging.TagManager;

@Component
@Service(TaggedLinkListService.class)
public class TaggedLinkListImpl implements TaggedLinkListService {

	private final Logger log = LoggerFactory.getLogger(TaggedLinkListImpl.class);
	private Session session;

	private TaggedLinkPage taggedLinkPage;

	private List<TaggedLinkPage> listtag;

	private String[] tags;

	private DayCalculator dayCalculator;

	public List<TaggedLinkPage> getTagList(String[] taglist, String includeAllTags, ResourceResolver resolver) {
		log.info("getTagList method started");
		try {

			session = resolver.adaptTo(Session.class);
			TagManager tagManager = resolver.adaptTo(TagManager.class);
			listtag = new ArrayList<TaggedLinkPage>();
			RangeIterator<Resource> itr = tagManager.find("/content", taglist, Boolean.valueOf(includeAllTags));
			while (itr.hasNext()) {
				taggedLinkPage = new TaggedLinkPage();
				String path = itr.next().getPath();
				Node node = session.getNode(path);
				String title = node.getProperty("jcr:title").getString();
				Calendar calLastModDate = node.getProperty("cq:lastModified").getDate();
				Date dateLastModDate = calLastModDate.getTime();
				dayCalculator = new DayCalculator();
				int result = dayCalculator.diffrenceDays(dateLastModDate);
				Property references = node.getProperty("cq:tags");
				Value[] values = references.getValues();
				tags = new String[values.length];
				for (int i = 0; i < values.length; i++) {
					tags[i] = values[i].getString();
				}
				if (result == 0 || result == 1) {
					taggedLinkPage.setPagePath(path);
					taggedLinkPage.setPageTitle(title);
					taggedLinkPage.setTags(tags);
					listtag.add(taggedLinkPage);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("getTagList method returning page information");
		return listtag;
		
	}
}