/**
* @Wipro Rights
 */
package com.aviva.tagapplication.service;

import java.util.Calendar;
import java.util.Date;

public class DayCalculator {
	/**
	 * Compares last modified date with current date
	 */
	public int diffrenceDays(Date dateLastModDate) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) - 4);
		Date fiveDaysBeforeDate = cal.getTime();
		int result = dateLastModDate.compareTo(fiveDaysBeforeDate);
		return result;
	}
}