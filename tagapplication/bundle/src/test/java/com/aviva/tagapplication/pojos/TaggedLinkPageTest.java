/**
 * @Wipro Rights
 */
package com.aviva.tagapplication.pojos;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TaggedLinkPageTest {

	private static final String TOOLBAR_PAGE_URL = "/content/geometrixx/en/toolbar";
	private static final String TOOLBAR_TITLE = "Toolbar";
	private static final String[] TAGS = { "tagnamespace:tagone", "tagnamespace:tagtwo", "tagnamespace:tagthree",
			"tagnamespace:tagfour" };
	private TaggedLinkPage taggedLinkPage;

	@Before
	public void setUp() throws Exception {
		taggedLinkPage = new TaggedLinkPage();
		taggedLinkPage.setPagePath(TOOLBAR_PAGE_URL);
		taggedLinkPage.setPageTitle(TOOLBAR_TITLE);
		taggedLinkPage.setTags(TAGS);
	}

	@Test
	public void testGetTitleOnPages() {
		assertEquals(TOOLBAR_TITLE, taggedLinkPage.getPageTitle());
	}

	@Test
	public void testGetPathOnPages() {
		assertEquals(TOOLBAR_PAGE_URL, taggedLinkPage.getPagePath());
	}

	@Test
	public void testGetTagsOnPages() {
		assertEquals(4, taggedLinkPage.getTags().length);
	}

	@After
	public void tearDown() throws Exception {
		taggedLinkPage = null;
	}
}
