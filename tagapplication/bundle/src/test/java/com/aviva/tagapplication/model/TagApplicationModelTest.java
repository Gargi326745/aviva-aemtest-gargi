/**
 * @Wipro Rights
 */
package com.aviva.tagapplication.model;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.ResourceResolver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.aviva.tagapplication.pojos.TaggedLinkPage;
import com.aviva.tagapplication.service.TaggedLinkListService;

@RunWith(MockitoJUnitRunner.class)
public class TagApplicationModelTest {

	private static final String TOOLBAR_PAGE = "/content/geometrixx/en/toolbar";
	private static final String PRODUCTS_PAGE = "/content/geometrixx/en/products";
	private static final String COMMUNITY_PAGE = "/content/geometrixx/en/community";
	private static final String COMPANY_PAGE = "/content/geometrixx/en/company";
	private static final String PRODUCTS_PAGE_TITLE = "Products";
	private static final String TOOLBAR_PAGE_TITLE = "Toolbar";
	private static final String COMMUNITY_PAGE_TITLE = "Community";
	private static final String COMPANY_PAGE_TITLE = "Company";
	private static final String TAG1 = "tagnamespace:tagone";
	private static final String TAG2 = "tagnamespace:tagtwo";
	private static final String TAG3 = "tagnamespace:tagthree";
	private static final String TAG4 = "tagnamespace:tagfour";
	private static final String[] TAG_ONE_ARRAY = new String[] { TAG1 };
	private static final String[] TAG_TWO_ARRAY = new String[] { TAG1, TAG2 };
	private static final String[] TAG_THREE_ARRAY = new String[] { TAG1, TAG2, TAG4 };
	private static final String[] TAG_FOUR_ARRAY = new String[] { TAG1, TAG2, TAG3, TAG4 };

	@Mock
	private ResourceResolver resourceResolver;

	@Mock
	private TagApplicationModel tagApplicationModel;

	@Mock
	private TaggedLinkListService taggedLinkListService;

	private List<TaggedLinkPage> taggedLinkPageActual;

	private String includeAllTags;

	TaggedLinkPage toolbarTaggedPage = new TaggedLinkPage(TOOLBAR_PAGE_TITLE, TOOLBAR_PAGE, TAG_ONE_ARRAY);
	TaggedLinkPage productsTaggedPage = new TaggedLinkPage(PRODUCTS_PAGE_TITLE, PRODUCTS_PAGE, TAG_TWO_ARRAY);
	TaggedLinkPage communityTaggedPage = new TaggedLinkPage(COMMUNITY_PAGE_TITLE, COMMUNITY_PAGE, TAG_THREE_ARRAY);
	TaggedLinkPage companyTaggedPage = new TaggedLinkPage(COMPANY_PAGE_TITLE, COMPANY_PAGE, TAG_FOUR_ARRAY);

	@Before
	public void setUp() throws Exception {
		includeAllTags = "false";
	}

	@Test
	public void testToGetEmptyResultWithoutInput() {
		taggedLinkPageActual = new ArrayList<TaggedLinkPage>();
		String[] TAG = new String[] {};
		includeAllTags = "true";
		when(taggedLinkListService.getTagList(TAG, includeAllTags, resourceResolver)).thenReturn(taggedLinkPageActual);
		assertEquals(0, taggedLinkListService.getTagList(TAG, includeAllTags, resourceResolver).size());
	}

	@Test
	public void testToGetSingleTagPageResult() {
		includeAllTags = "true";
		taggedLinkPageActual = new ArrayList<TaggedLinkPage>();
		taggedLinkPageActual.add(toolbarTaggedPage);
		when(taggedLinkListService.getTagList(TAG_ONE_ARRAY, includeAllTags, resourceResolver))
				.thenReturn(taggedLinkPageActual);
		assertEquals(1, taggedLinkListService.getTagList(TAG_ONE_ARRAY, includeAllTags, resourceResolver).size());
	}

	@Test
	public void testToGetTwoTagPageResult() {
		includeAllTags = "true";
		taggedLinkPageActual = new ArrayList<TaggedLinkPage>();
		taggedLinkPageActual.add(toolbarTaggedPage);
		taggedLinkPageActual.add(productsTaggedPage);
		when(taggedLinkListService.getTagList(TAG_TWO_ARRAY, includeAllTags, resourceResolver))
				.thenReturn(taggedLinkPageActual);
		assertEquals(2, taggedLinkListService.getTagList(TAG_TWO_ARRAY, includeAllTags, resourceResolver).size());
	}

	@Test
	public void testToGetMultipleTagPageResult() {
		includeAllTags = "true";
		taggedLinkPageActual = new ArrayList<TaggedLinkPage>();
		taggedLinkPageActual.add(toolbarTaggedPage);
		taggedLinkPageActual.add(productsTaggedPage);
		taggedLinkPageActual.add(communityTaggedPage);
		taggedLinkPageActual.add(companyTaggedPage);
		when(taggedLinkListService.getTagList(TAG_FOUR_ARRAY, includeAllTags, resourceResolver))
				.thenReturn(taggedLinkPageActual);
		assertEquals(4, taggedLinkListService.getTagList(TAG_FOUR_ARRAY, includeAllTags, resourceResolver).size());
	}

	@After
	public void tearDown() throws Exception {
		taggedLinkPageActual = null;

	}
}
